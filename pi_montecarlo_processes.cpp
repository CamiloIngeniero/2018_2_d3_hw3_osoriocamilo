#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <math.h>

pid_t *pidChildren;
int n, id;//n=numero de hijos//
void *smem;

int find_child(pid_t pid){
    for(int i=0; i<n; i++){
        if(pid == pidChildren[i])
            return i;
    }
    return -1;
}

int main(int argc, char** argv)
{
    int i, numInters;
    float Aci=0,Acu=0;
    int PosX, PosY;
    float pii;
    


    if(argc==3){
        numInters = atoi(argv[1]);
        n = atoi(argv[2]);
        printf("[P] Iniciando con %d iteraciones y %d procesos.\n", numInters, n);
    }else if(argc==1){
        printf("HALLAR PI CON EL METODO MONTECARLO CON HILOS\n");
        printf("¿Cùantos hijos desea?");
        scanf("%d",&n);
        printf("¿Cùantos puntos desea ingresar al area del cuadro?");
        scanf("%d",&numInters);
        printf("[P] Iniciando con %d iteraciones y %d procesos.\n", numInters, n);
    }else{
        printf("[P] Error. Número incorrecto de parámetros.\n");
        return 1;
    }

    smem = mmap(NULL, n*sizeof(float), PROT_READ | PROT_WRITE,
                    MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    printf("[n] Creando %d hijos.\n\n", n);
    pidChildren = new pid_t[n];

    for(i=0; i<n; i++){
        pidChildren[i] = fork();
        if(pidChildren[i] == 0){
            id = i;
            printf("[C%d] Hijo creado.\n", id);
            break;
        }
    }

    if(i==n){
        double totalPi = 0.0;
        printf("[P] Esperando finalización de hijos.\n");
        for(i=0; i<n; i++){
            int status;
            pid_t wpid = wait(&status);
            int wid = find_child(wpid);
            printf("[P] Finalizado el hijo %d\n", wid);
        }
        for(i=0; i<n; i++){
            totalPi += *(((double*)smem)+i);
        }
        totalPi *= 4.0;
        munmap(smem, n*sizeof(float));
        printf("[P] Fin de proceso padre.\n");
        return 0;
    }else{
            srand(time(0));
    for (int i = 0; i < numInters; ++i)
    {
        PosX = 0 + rand() % (01+100);
        PosY = 0 + rand() % (01+100);
        PosX = PosX-50;
        PosY = PosY-50;
         //printf("x: %d\n",PosX );
         //printf("y: %d\n",PosY );

        float Punto=sqrt((PosX*PosX)+(PosY*PosY));
        //printf("diametro: %f\n",Punto);

        if (Punto<=50)//DistanciaPuntoCentro<=Diametro/2
        {
            //printf("Estoy adentro\n");
            Aci = Aci+1;
            Acu = Acu+1;
        }
        else{
            //printf("Estoy afuera\n");
            Acu = Acu+1;
        }    
    }
        pii=((4*Aci)/Acu);
        printf("Pi = %f\n",pii);
        }
        return 0;
    }


