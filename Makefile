all: pi_montecarlo_processes

pi_montecarlo_processes: pi_montecarlo_processes.cpp
	g++ -o pi_montecarlo_processes pi_montecarlo_processes.cpp -lrt

.PHONY: clean
clean:
	rm -rf pi_montecarlo_processes

